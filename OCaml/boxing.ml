exception BoxingFailure of string

type item =
  | Gift
  | Box of item list

let rec string_of_item = function
  | Gift -> "Gift"
  | Box (items) ->
      let items_str = String.concat ";" (List.map string_of_item items)
      in "Box[" ^ items_str ^ "]"

let compute_step stack = function
  | 'b' -> Box [] :: stack
  | 'g' -> Gift :: stack
  | 'p' -> 
      (match stack with
       | Box (elems) :: el :: tail -> Box (el :: elems) :: tail
       | Box (_) :: [] -> raise (BoxingFailure "Nothing to put inside a Box")
       | Gift :: _ -> raise (BoxingFailure "Can't put anything inside a Gift")
       | [] -> raise (BoxingFailure "Stack is empty"))
  | _ -> raise (BoxingFailure "Unsupported character")
                 
let compute xs =
  let stack = String.fold_left compute_step [] xs in
  match stack with
  | res :: [] -> string_of_item res
  | _ -> raise (BoxingFailure "Non empty final stack")

let input = "gbpbpbbbpbpppbp"
let expected = "Box[Box[Box[Box[Gift]];Box[];Box[Box[]]]]"

let () = match compute input with
  | res -> print_endline ("Success:" ^ res)
  | exception BoxingFailure (msg) -> prerr_endline ("BoxingFailure: " ^ msg)
