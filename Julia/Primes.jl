function solution(n::Integer, m::Integer)::String
    top = floor(Int, sqrt(m))
    arr = trues(m)
    arr[1] = false

    for i in 2:top
        if arr[i]
            for j in i^2:i:m
                arr[j] = false
            end
        end
    end

    primes = findall(arr)
    n_idx = findfirst(x -> x > n, primes)
    res = primes[n_idx:length(primes)]
    
    return join(res, ' ')
end

n = parse(Int, readline())
m = parse(Int, readline())
println(solution(n, m))