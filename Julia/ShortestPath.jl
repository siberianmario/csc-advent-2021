using LinearAlgebra
using Plots

struct Line{T <: Real}
  a::Vector{T}
  b::Vector{T}
  n::Vector{T}
end

function withinLine(p::Vector{T}, l::Line{T}) where {T <: Real}
  (p >= l.a && p <= l.b) || (p >= l.b && p <= l.a)
end

function shortestPath(p::Vector{T}, l::Line{T}) where {T <: Real}
  a, b, n = l.a, l.b, l.n
  pa = a - p
  path = pa - ((pa ⋅ n)*n)
  dest = p + path
  if withinLine(dest, l)
    (norm(path), dest)
  else
    distA = norm(p - a)
    distB = norm(p - b)
    distA < distB ? (distA, a) : (distB, b)
  end
end

function parsePoints(lines = Vector{String})
  n = parse(Int, lines[1])
  points = map(l -> map(s -> parse(Float64, s), split(l)),lines[2:n+1])
  points, lines[n+2:end]
end

function makeLine(pair::Vector{Vector{T}}) where {T <: Real}
  a, b = pair
  n = (b - a) / norm(b - a)
  Line(a, b, n)
end

function destination(p::Vector{T}, lines::Vector{Line{T}}) where {T <: Real}
  paths = [shortestPath(p, line) for line in lines]
  minimum(paths)[2]
end

input = readlines("./Julia/path_input2.txt")

pathPoints, tail = parsePoints(input)
cities, _ = parsePoints(tail)

pathLines = [makeLine(pathPoints[i:i+1]) for i in 1:length(pathPoints)-1]
destinations = [destination(city, pathLines) for city in cities]

for dest in destinations
  coord = map(v -> round(v, digits=3), dest)
  println(join(coord, " "))
end

xmasPlot = plot(getindex.(pathPoints,1), getindex.(pathPoints,2), legend=false, aspect_ratio=1, grid=false)
scatter!(getindex.(cities,1), getindex.(cities,2), markercolor = :green)
scatter!(getindex.(destinations,1), getindex.(destinations,2), markercolor = :red)

savefig(xmasPlot,"./Julia/xmasPlot.png")