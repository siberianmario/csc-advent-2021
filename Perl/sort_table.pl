# put your perl code here
@lines = <>;
@tail = @lines[2..$#lines-1];
chomp @tail;

@workers;
foreach $line (@tail) {
  $line = lc $line;
  $line =~ s/\b(.)/\u\1/g;
  my @fields = split ',', $line;
  next if (@fields < 6);
  push @workers, \@fields;
}

@sorted = sort {@$a[5] <=> @$b[5]} @workers;

$age = 0;
$salary = 0;
for ($i = 0; $i < @sorted; $i++) {
  $fields_ref = $sorted[$i];
  @fs = @$fields_ref;
  $age += @fs[2];
  $salary += @fs[5];
  printf "%4d | %-16s | %-3s | %-24s | %-9s\n", $i+1, "@fs[1] @fs[0]", @fs[2], @fs[4], @fs[5];
}

printf "\nAvg age: %d\nAvg salary: %d\n", $age / @sorted, $salary /@sorted;
