typealias Function = (Double) -> Double

/**
 * Интерполируем функцию по набору значений [xs] и [ys]. Длины наборов должны быть одинаковыми
 */
fun interpolate(xs: List<Double>, ys: List<Double>): Function {
    require(xs.size == ys.size) { "Длина набора xs - ${xs.size}, а набора ys - ${ys.size}" }
    // return an anonymous function
    return { x ->
        // check interpolation region. Zero outside the region
        if (x < xs.first() || x > xs.last()) 0.0 else {
            // find section number
            val num: Int = xs.indexOfFirst { it >= x }
            // num >= 0
            if (num < 0) {
                0.0
            } else if (num == 0) {
                ys[0]
            } else {
                //return the result as last expression
                ys[num - 1] + (ys[num] - ys[num - 1]) / (xs[num] - xs[num - 1]) * (x - xs[num - 1])
            }
        }
    }
}

fun convolution(xs: List<Double>, f: Function, g: Function): Function {
    val ys = xs.map { x ->
        integrate(xs, { y -> f(y) * g(x - y) })
    }
    return interpolate(xs, ys)
}

fun integrate(xs: List<Double>, f: Function): Double {
    var result = 0.0
    val ys = xs.map { f(it) }
    for (i in 1..xs.size - 1) {
        result += (ys[i - 1] + ys[i]) * (xs[i] - xs[i - 1]) / 2
    }
    return result
}

fun main() {
    val n = readLine()!!.trim().toInt()
    val xs: List<Double> = readLine()!!.split(" ").map { it.toDouble() }
    val ys: List<Double> = readLine()!!.split(" ").map { it.toDouble() }

    val f = interpolate(xs, ys)
    val fs = List(n) { f }

    val conv = fs.reduce { acc, g -> convolution(xs, acc, g) }

    val res: List<Double> = xs.map { conv(it) }
    println(res.joinToString(separator = " "))
}