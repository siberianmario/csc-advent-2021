Inductive nat : Type :=
  | O : nat
  | S : nat -> nat.

Check O.
Check S O.
Check S (S O).

Fixpoint add (m n : nat) : nat :=
  match m with
  | O => n
  | S m' => S (add m' n)
  end.

Compute add (S (S O)) (S (S O)).
Compute add (S (S O)) O.
Compute add O (S (S O)).

Notation "m + n" := (add m n).

Lemma addA : forall m n p : nat,
  (m + n) + p = m + (n + p).
Proof.
  intros m n p.
  induction m as [ | m' IHm ].
  - reflexivity.
  - simpl. rewrite IHm. reflexivity.
Qed.

Lemma addn0 : forall n : nat,
  n + O = n.
Proof.
  intros n.
  induction n as [ | n' IHn ].
  - reflexivity.
  - simpl. rewrite IHn. reflexivity.
Qed.

Lemma addnS : forall m n : nat,
  m + S n = S (m + n).
Proof.
  intros m n.
  induction m as [ | m' IHm ].
  - reflexivity.
  - simpl. rewrite IHm. reflexivity.
Qed.

Lemma addC : forall m n : nat,
  m + n = n + m.
Proof.
  intros m n.
  induction m as [ | m' IHm ].
  - simpl. rewrite addn0. reflexivity.
  - simpl. rewrite IHm. rewrite addnS. reflexivity.
Qed.

Lemma addCA : forall m n p,
  m + (n + p) = n + (m + p).
Proof.
  intros m n p.
  rewrite <- addA.
  rewrite (addC m n).
  rewrite addA.
  reflexivity.
Qed.

(* hemework *)

Fixpoint mul (m n : nat) : nat :=
  match m with
  | O => O
  | S m' => n + mul m' n
  end.

Notation "m * n" := (mul m n).

Lemma muln0 : forall n : nat,
  n * O = O.
Proof.
  intros n.
  induction n as [ | n' IHn ].
  - reflexivity.
  - simpl. rewrite IHn. reflexivity.
Qed.

Lemma mulnS : forall m n : nat,
  m * S n = m + m * n.
Proof.
  intros m n.
  induction m as [ | m' IHm ].
  - reflexivity.
  - simpl. rewrite IHm. rewrite addCA. reflexivity.
Qed.

Lemma mulC : forall m n : nat,
  m * n = n * m.
Proof.
  intros m n.
  induction m as [ | m' IHm].
  - simpl. rewrite muln0. reflexivity.
  - simpl. rewrite IHm. rewrite mulnS. reflexivity.
Qed.

Lemma mulD : forall m n p : nat,
  p * (m + n) = p * m + p * n.
Proof.
  intros m n p.
  induction p as [ | p' IHp ].
  - reflexivity.
  - simpl.
    rewrite IHp.
    rewrite (addA m (p' * m) (n + p' * n)).
    rewrite (addCA (p' * m) n (p' * n)).
    repeat rewrite <- addA.
    reflexivity.
Qed.

Lemma mulA : forall m n p : nat,
  (m * n) * p = m * (n * p).
Proof.
  intros m n p.
  induction m as [ | m' IHm ].
  - reflexivity.
  - simpl.
    rewrite mulC.
    rewrite mulD.
    rewrite <- IHm.
    rewrite mulC.
    rewrite (mulC p (m' * n)).
    reflexivity.
Qed.