import sys
from math import ceil, floor
from copy import copy

class Coord:
    def __init__(self, top, bot, left, right):
        self.top = top
        self.bot = bot
        self.left = left
        self.right = right

    def __copy__(self):
        return Coord(self.top, self.bot, self.left, self.right)

    def __hash__(self):
        return hash((self.top, self.bot, self.left, self.right))

    def __eq__(self, other):
        return (self.top, self.bot, self.left, self.right) == (other.top, other.bot, other.left, other.right)

    def __ne__(self, other):
        return not(self == other)
    
    def __repr__(self):
        report = "Coord(top={}, bot={}, left={}, right={})"
        return report.format(self.top, self.bot, self.left, self.right)

    def area(self):
        return (self.bot - self.top) * (self.right - self.left)

def get_moves(cs):
    moves = []
    v_mean = cs.top + (cs.bot - cs.top) / 2
    h_mean = cs.left + (cs.right - cs.left) / 2

    for top in range(cs.top + 1, floor(v_mean) + 1):
        move = copy(cs)
        move.top = top
        moves.append(move)

    for bot in range(ceil(v_mean), cs.bot):
        move = copy(cs)
        move.bot = bot
        moves.append(move)

    for left in range(cs.left + 1, floor(h_mean) + 1):
        move = copy(cs)
        move.left = left
        moves.append(move)

    for right in range(ceil(h_mean), cs.right):
        move = copy(cs)
        move.right = right
        moves.append(move)

    return moves

def finished(table, cs):
    ones = 0
    for row in table[cs.top: cs.bot]:
        ones += sum(row[cs.left: cs.right])
    return ones == cs.area() or ones == 0

def next_player(player):
    return 0 if player == 1 else 1

def print_table(table, cs):
    for row in table[cs.top: cs.bot]:
        print(row[cs.left: cs.right])

if __name__ == '__main__':
    m = [[(1 if num == 'F' else 0) for num in list(line.strip('\n'))] for line in sys.stdin]
    moves_dict = {}

    def next_move(cs, player):
        # print_table(m, cs)
        if finished(m, cs):
            # print("finished at {} with player {}".format(cs, player))
            return m[cs.top][cs.left]
        else:
            other_player = next_player(player)
            moves = get_moves(cs)
            winners = []
            for move in moves:
                if move in moves_dict:
                    if moves_dict[move][player] is not None:
                        # print("reading from dictionary for {} with player {}".format(move, player))
                        winner = moves_dict[move][player]
                    else:
                        # print("computing outcome for {} again, but with player {}".format(move, player))
                        winner = next_move(move, other_player)
                        moves_dict[move][player] = winner
                else:
                    # print("computing outcome for {} with player {} for the first time".format(move, player))
                    winner = next_move(move, other_player)
                    record = [None, None]
                    record[player] = winner
                    moves_dict[move] = record
                winners.append(winner)
            return player if player in winners else other_player

    winner = next_move(Coord(0, len(m), 0, len(m[0])), 1)
    if winner == 1:
        print("YES")
    else:
        print("NO")

    # print(moves_dict)