local M = {}
function M.invsin(x)
  return 1 / math.sin(x)
end
return M