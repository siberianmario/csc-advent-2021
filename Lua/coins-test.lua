coins = require("coins")

local solver = coins.solver({ 1, 2, 5 })

for i = 1, 25, 1 do
    print(solver(i))
end