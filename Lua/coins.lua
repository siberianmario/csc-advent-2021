local M = {}
function M.solver(coins)

  return function(value)
    local min = nil
    table.sort(coins)

    function _solver(amount, n)
      if amount == 0 then
        if min == nil then
          min = n
        else
          min = math.min(min, n)
        end
      else
        for i = 1, #coins, 1 do
          if coins[i] <= amount then
            _solver(amount - coins[i], n + 1)
          else
            break
          end
        end
      end
    end

    _solver(value, 0)
    return min
  end

end
return M

