function expel(x)
  return function(t, n)
      if x <= 1 then
          return
      end
      for i = x * 2, n, x do
          t[i] = nil
      end
  end
end

local N = 100
local numbers = {}
local pipeline = {}

for i = 1, N, 1 do
  table.insert(numbers, i)
  pipeline[i] = expel(i)
end

for k, func in ipairs(pipeline) do
  func(numbers, N)
end

local sum = 0
for k, v in pairs(numbers) do
  sum = sum + v
end

print(sum)