open FSharp.Data

type SibSUTI = 
    HtmlProvider<
        "https://sibsutis.ru/applicants/abiturient/places/">

let sample = SibSUTI.GetSample ()

let lossAdd x y = x + 0.8 * y

let potentialStudentCount = 
    sample.Tables.``Количество мест``.Rows
    |> Seq.map (fun row ->
        row.``бюджетные места - Всего бюджетных мест``,
        row.``План приема на платные места (на договорной основе)``)
    |> Seq.map (fun (budget, paid) ->
        (if budget.HasValue then budget.Value else 0) + paid)
    |> Seq.sum
    |> Seq.replicate 6
    |> Seq.reduce lossAdd