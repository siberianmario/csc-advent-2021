let rec fibonacci' n0 n1 = function
  | 0 -> n0 + n1
  | n -> fibonacci' n1 (n0 + n1) (n - 1)

let fibonacci = function
  | 0 -> 0
  | 1 -> 1
  | x -> fibonacci' 0 1 (x - 2)

let fibs = List.map fibonacci [0..10]

printfn "%A" fibs
