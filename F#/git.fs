type Command =
    | Clone of Branch: string
    | Checkout of Branch: string * Reset: bool

type ConfigOption = {
    Key: string
    Value: string
}

type CommandLineOptions = {
    ConfigOptions: ConfigOption list
    Command: Command option
}

let emptyOptions = {
    ConfigOptions = []
    Command = None
}

let input = ["--config"; "test"; "1"; "checkout"; "origin/mybranch"]

let rec parseConfig' config terminators = function
    | [] -> config, []
    | str :: tail when List.contains str terminators ->
        config, str :: tail
    | k :: v :: tail ->
        let confOpt = { Key = k; Value = v }
        parseConfig' (confOpt :: config) terminators tail
    | _ -> failwith "Invalid options list" 

let parseConfig = function
    | "--config" :: lst ->
        let config, tail = parseConfig' [] ["checkout"; "clone"] lst
        List.rev config, tail
    | lst -> [], lst

let parseCommand = function
    | "clone" :: branch :: _ -> Some(Clone branch)
    | "checkout" :: branch :: tail ->
        let reset = Option.contains "-reset" (List.tryHead tail)
        Some(Checkout(branch, reset))
    | _ -> None

let parseCommandLine(args: string list): CommandLineOptions =
    let options, tail = parseConfig args
    let command = parseCommand tail
    { ConfigOptions = options; Command = command }

let compress args =
    parseCommandLine args
    |> string
    |> (fun f -> f.Replace("\n", ";").Replace(" ", ""))

printfn "%s" (compress input)