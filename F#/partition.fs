let rec partition' p hs ts = function
    | []         -> List.rev hs @ ts
    | el :: tail ->
        if p el then
            partition' p hs (el :: ts) tail
        else
            partition' p (el :: hs) ts tail

let partition predicate =
    partition' predicate [] []

printf "%A" (partition (fun x -> x > 3) [5; 2; 1; 4])

let lst = ["A"; "B"; "C"]
printfn "%A" (partition (fun s -> String.length s >= 4) lst)
