type Expression =
    | Plus of Expression * Expression
    | Minus of Expression * Expression
    | Multiply of Expression * Expression
    | Divide of Expression * Expression
    | Literal of float

let expr =
    let ``5`` = Literal 5.
    let ``50`` = Literal 50.
    let h = (Plus(``50``, ``50``))
    let a = Plus ((Divide ((Plus(``50``, ``5``)), ``5``)), ``5``)
    Divide(Plus(h, Divide(Multiply(Plus(a, a), ``50``), ``5``)), Literal 10.)

let rec calculate = function
    | Plus(a, b)     -> calculate a + calculate b
    | Minus(a, b)    -> calculate a - calculate b
    | Multiply(a, b) -> calculate a * calculate b
    | Divide(a, b)   -> calculate a / calculate b
    | Literal a      -> a

printf "%f" (calculate expr)