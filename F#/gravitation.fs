[<Measure>] type kg
[<Measure>] type m
[<Measure>] type s
[<Measure>] type N = kg * m / s^2

let G = 6.6743e-11<N * m^2 / kg^2>

let F(m1: float<kg>, m2: float<kg>, r: float<m>): float<N> =
    G * (m1 * m2) / (r * r)

let result = F(1000000.0<kg>, 1000000.0<kg>, 4.0<m>)
printf "%f" result